﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class Scoreboard : MonoBehaviourPunCallbacks
{
    public static Scoreboard instance;
    

    [SerializeField] Transform container;
    [SerializeField] Transform scoreboardItemPrefab;


    Dictionary<Player, ScoreboardItem> scoreboardItems = new Dictionary<Player, ScoreboardItem>();
    public Dictionary<Player, int> playersKillScore = new Dictionary<Player, int>();

    public Player player;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    void Start() 
    {
        foreach(Player player in PhotonNetwork.PlayerList)
        {
           
            AddScoreboardItem(player);
        }

    }


    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddScoreboardItem(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        RemoveScoreboardItem(otherPlayer);
    }

    void AddScoreboardItem(Player player)
    {
        ScoreboardItem item = Instantiate(scoreboardItemPrefab, container).GetComponent<ScoreboardItem>();
        item.Initialize(player);
        scoreboardItems[player] = item;
        var players = FindObjectsOfType<PlayerSetup>();
        foreach (PlayerSetup i in players)
        {
            if(i.photonView.Owner == player)
            {
                i.scoreboardItem = item;
            }
        }
        playersKillScore.Add(player, 0);
    }

    void RemoveScoreboardItem(Player player)
    {
        Destroy(scoreboardItems[player].gameObject);
        scoreboardItems.Remove(player);
    }

    public void UpdateScore()
    {
        foreach (Player p in PhotonNetwork.PlayerList)
        {
           
           scoreboardItems[p].killCountText.text = playersKillScore[p].ToString();

        }
        player = GetWinnerScore();
      //  ScoreDate(player, score);
        
    }

    [PunRPC]
    public void ScoreDate()
    {
        foreach (Player p in PhotonNetwork.PlayerList)
        {
          
           scoreboardItems[p].killCountText.text = scoreboardItems[p].killCountText.text;
            


        }
    }

    public Player GetWinnerScore()
    {
        foreach(Player p in PhotonNetwork.PlayerList)
        {
            if(playersKillScore[p] >= 10)
            {
                return p;
            }
        }

        return null;
    }
}
