﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityStandardAssets.Characters.FirstPerson;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public GameObject nonFpsModel;
    public GameObject playerUIPrefab;

    public PlayerMovementController playerMovementController;
    public Camera fpsCamera;

    private Animator animator;
    public Avatar fpsAvatar, nonFpsAvatar;

    private Shooting shooting;

    public TextMeshProUGUI nameHolder;

    GameManager gameManager;
    SpawnerScript spawnerScript;
    Scoreboard scoreboard;
    public ScoreboardItem scoreboardItem;
    int killScore = 0;
    // Start is called before the first frame update

    void Start()
    {
        spawnerScript = SpawnerScript.Instance;
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        playerMovementController = this.GetComponent<PlayerMovementController>();

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        animator = this.GetComponent<Animator>();
        animator.SetBool("isLocalPlayer", photonView.IsMine);

        shooting = this.GetComponent<Shooting>();
        animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;
      
        if(photonView.IsMine)
        {
            photonView.Owner.SetScore(0);
            gameManager.PlayerKillCount.Add(photonView.Owner.NickName, 1);
            Debug.Log(gameManager.PlayerKillCount[photonView.Owner.NickName] + " name:" + photonView.Owner.NickName);

            int randNum = Random.Range(0, spawnerScript.spawnPoints.Capacity);
            this.transform.position = spawnerScript.GetSpawnPoint().position;

            GameObject playerUI = Instantiate(playerUIPrefab);
            playerMovementController.fixedTouchField = playerUI.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerUI.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            fpsCamera.enabled = true;
            
            nameHolder.text = PhotonNetwork.NickName;
            //Adding OnClick Listener
            playerUI.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shooting.Fire());
        }

        else
        {
            photonView.Owner.SetScore(0);
            nameHolder.text = photonView.Owner.NickName;
            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
        }
    }

    public void AddKillCount()
    {
        if(photonView.IsMine)
        {
            killScore++;
        }
    }
}
