﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthbar;

    private Animator animator;

    [Header("Kill Listing")]
    public GameObject killListingPrefab;
    public GameObject killFeedPanel;

    SpawnerScript spawnerScript;
    private int killCount;
    GameManager gameManager;

    Scoreboard scoreboard;
  

   // public Text howDisplay;
    // Start is called before the first frame update
    void Start()
    {

        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameManager = GameManager.instance;
        spawnerScript = SpawnerScript.Instance;

        scoreboard = Scoreboard.instance;

        killCount = 0;
        health = startHealth;
        healthbar.fillAmount = health / startHealth;

        animator = this.GetComponent<Animator>();
        killFeedPanel = GameObject.FindGameObjectWithTag("KillFeedPanel");
        
    }


   
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthbar.fillAmount = health / startHealth;
        
        if(health <= 0 )
        {   
            //Die()
            photonView.RPC("Die", RpcTarget.All);
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);

           
            if(gameManager.PlayerKillCount.ContainsKey(info.Sender.NickName))
            {
                //Add 1 Kill to killer  
                
                gameManager.PlayerKillCount[info.Sender.NickName] = gameManager.PlayerKillCount[info.Sender.NickName] + 1;
               // scoreboard.GetScore(info.Sender.Id)
                
                Debug.Log(gameManager.PlayerKillCount[info.Sender.NickName] + " name:" + info.Sender.NickName);

                //scoreboard.UpdateScore(info.Sender, gameManager.PlayerKillCount[info.Sender.NickName]);
                photonView.RPC("AddKillScore", RpcTarget.AllBuffered, info.Sender);
                
                
            }
            photonView.RPC("AddNewKillListing", RpcTarget.All, info.Sender.NickName, info.photonView.Owner.NickName);
            //killFeedPanel.GetComponent<KillFeed>().AddNewKillListing(info.Sender.NickName, info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    [PunRPC]
    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

  

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText"); 
        float respawnTime = 5; 

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;


            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");

        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";
        

        int randNum = Random.Range(0, spawnerScript.spawnPoints.Count);
        this.transform.position = spawnerScript.GetSpawnPoint().position;
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);


    }

    [PunRPC]
    public void RegainHealth()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
    }



    [PunRPC]
    public void AddNewKillListing(string killer, string killed)
    {
        Debug.Log("Called Kill List");
        GameObject temp = Instantiate(killListingPrefab, killFeedPanel.transform);
        temp.transform.SetSiblingIndex(0);
        KillListing tempListing = temp.GetComponent<KillListing>();

        tempListing.killedDisplay.text = killed;
        tempListing.killerDisplay.text = killer;
        tempListing.howDisplay.text = "Shot";

        Destroy(temp, 3f); 
      //  photonView.RPC("SetNames", RpcTarget.All, killer, killed);

//        tempListing.SetNames(killer, killed);

    }

    [PunRPC]
    void AddKillScore(Player player)
    {
        scoreboard.playersKillScore[player]++;
        scoreboard.UpdateScore();
        gameManager.playerWinner = scoreboard.GetWinnerScore();
    }
}
