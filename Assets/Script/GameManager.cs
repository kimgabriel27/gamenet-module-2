﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using TMPro;
public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    public static GameManager instance;
    // Start is called before the first frame update
    SpawnerScript spawners;

    public Dictionary<string, int> PlayerKillCount = new Dictionary<string, int>();
    public bool isPaused = false;

    public GameObject scoreboardGameObject;
    Scoreboard scoreboardScript;

    public TMP_Text messageText;
    public List<Player> playersInGame = new List<Player>();
    public Player playerWinner = null;
    public static readonly byte RestartGameEventCode = 1;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        messageText.text = "";
        spawners = SpawnerScript.Instance;
        scoreboardScript = Scoreboard.instance;
        if(PhotonNetwork.IsConnectedAndReady)
        {
            int randNum = Random.Range(0, spawners.spawnPoints.Capacity);
            GameObject g = PhotonNetwork.Instantiate(playerPrefab.name, spawners.spawnPoints[randNum].transform.position , Quaternion.identity);
          //  photonView.Owner.TagObject = g;
       
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            scoreboardGameObject.SetActive(true);
        }
        if(Input.GetKeyUp(KeyCode.Tab))
        {
            scoreboardGameObject.SetActive(false);
        }

        //CheckWinner();
        if(scoreboardScript.player != null)
        {
            //photonView.RPC("CheckWinner", RpcTarget.AllBuffered);
            StartCoroutine(DisplayMessage(scoreboardScript.player.NickName + " Wins"));
        }
       
    }

    [PunRPC]
    void CheckWinner()
    {
        if (scoreboardScript.GetWinnerScore() != null)
        {
            if (photonView.Owner == scoreboardScript.GetWinnerScore())
            {
                StartCoroutine(DisplayMessage(photonView.Owner.NickName + " Wins"));
            }

        }

        StartCoroutine(DisplayMessage(photonView.Owner.NickName + " Wins"));
    }

    //void RestartGame()
    //{
    //    PhotonNetwork.RaiseEvent()
    //}

    IEnumerator DisplayMessage(string message)
    {
        messageText.text = message;
        yield return new WaitForSeconds(5);
        messageText.text = "";
        scoreboardScript.player = null;
        
    }

    
}
