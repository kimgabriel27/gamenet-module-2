﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class KillListing : MonoBehaviourPunCallbacks
{
    public Text killerDisplay; // UI Text for The Killer name
    public Text killedDisplay; // UI Text for the killed name

    public Text howDisplay;
   
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 10f);
    }

 
    public void SetNames(string killerName, string killedName)
    {
        killedDisplay.text = killedName;
        killerDisplay.text = killerName;

        howDisplay.text = "Shot";
    }
}
