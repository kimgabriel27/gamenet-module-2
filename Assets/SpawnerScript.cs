﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    private static SpawnerScript instance;
    public static SpawnerScript Instance { get { return instance; } }

    public List<GameObject> spawnPoints = new List<GameObject>();
    public GameObject[] pointsSpawn;
    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public Transform GetSpawnPoint()
    {
        return spawnPoints[Random.Range(0, pointsSpawn.Length)].transform;
    }
}
